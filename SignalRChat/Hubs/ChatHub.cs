﻿using Microsoft.AspNetCore.SignalR;
using SignalRChat.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalRChat.Hubs
{
    public class ChatHub : Hub
    {
        public static ControlPanel panel;

        public async Task SendMessage(string userId, string message)
        {
            if (int.TryParse(userId, out int userIdInt) && panel.IsOnline(userIdInt))
            {
                panel.AddMessage(userIdInt, message, DateTime.UtcNow);
                await Clients.Caller.SendAsync("ReceiveMessage", userId, message, DateTime.UtcNow);
                await Clients.OthersInGroup("LoggedIn").SendAsync("ReceiveMessage", userId, message, DateTime.UtcNow);
            }
        }

        public async Task Login(string user)
        {
            var loginPkg = panel.SignIn(user);
            await Groups.AddToGroupAsync(Context.ConnectionId, "LoggedIn");
            await Clients.Caller.SendAsync("LoginSuccess", loginPkg);
        }

        public async Task LeftClick(int userId, int x, int y)
        {
            if (panel.LeftClick(userId, x, y))
            {
                await Clients.Group("LoggedIn").SendAsync("UpdateGameState", panel.GetMinesweeperState());
            }
        }
    }
}
