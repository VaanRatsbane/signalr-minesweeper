﻿using SignalRChat.Logic.Minesweeper;
using SignalRChat.Logic.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalRChat.Logic
{
    public class ControlPanel
    {
        private int UserIdSequence;
        private ConcurrentDictionary<int, User> Users;
        private ConcurrentDictionary<int, User> LoggedInUsers;

        private const int ChatQueueSize = 30;
        private ConcurrentQueue<ChatMessage> ChatMessages;

        Minesweeper.Minesweeper minesweeper;

        public ControlPanel ()
        {
            UserIdSequence = 1;
            Users = new ConcurrentDictionary<int, User>();
            LoggedInUsers = new ConcurrentDictionary<int, User>();

            ChatMessages = new ConcurrentQueue<ChatMessage>();
            minesweeper = new Minesweeper.Minesweeper();
        }

        public LoginPackage SignIn(string username)
        {
            var user = Users.Values.FirstOrDefault(c => c.Name == username);
            if(user is null)
            {
                int newId;
                do
                {
                    newId = UserIdSequence++;
                } while (Users.ContainsKey(newId));

                user = new User()
                {
                    ID = newId,
                    Name = username
                };

                if(!Users.TryAdd(newId, user))
                    throw new InvalidOperationException();
            }

            if (!LoggedInUsers.ContainsKey(user.ID))
                if (!LoggedInUsers.TryAdd(user.ID, user))
                    throw new InvalidOperationException();

            return new LoginPackage()
            {
                ID = user.ID,
                MessageLog = ChatMessages,
                OnlineUsers = LoggedInUsers.Values,
                MinesweeperCols = Minesweeper.Minesweeper.COLS,
                MinesweeperRows = Minesweeper.Minesweeper.ROWS
            };
        }

        public IEnumerable<User> GetOnlineUsers()
        {
            return LoggedInUsers.Values;
        }

        public bool IsOnline(int userId)
        {
            return LoggedInUsers.ContainsKey(userId);
        }

        public User GetUser(int ID)
        {
            User user = null;
            Users.TryGetValue(ID, out user);
            return user;
        }

        public void AddMessage(int userId, string message, DateTime utcNow)
        {
            ChatMessages.Enqueue(new ChatMessage()
            {
                Content = message,
                Sent = utcNow,
                UserId = userId
            });

            if (ChatMessages.Count > ChatQueueSize)
                ChatMessages.TryDequeue(out ChatMessage dequeued);
        }

        public bool LeftClick(int userId, int x, int y)
        {
            if (!IsOnline(userId))
                return false;

            return minesweeper.Click(x, y);
        }

        public GameState GetMinesweeperState()
        {
            return minesweeper.GameState();
        }
    }
}
