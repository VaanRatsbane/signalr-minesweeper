﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalRChat.Logic.Minesweeper
{
    public class GameState
    {
        public int[] BoardView;
        public int this[int key]
        {
            get
            {
                return BoardView[key];
            }
            set
            {
                BoardView[key] = value;
            }
        }

        public bool Lost { get; set; }
        public bool Won { get; set; }

        public GameState(int rows, int cols)
        {
            BoardView = new int[rows * cols];
        }
    }
}
