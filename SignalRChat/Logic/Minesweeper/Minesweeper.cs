﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalRChat.Logic.Minesweeper
{
    public class Minesweeper
    {
        public const int ROWS = 16;
        public const int COLS = 30;
        const int MINES = 99;

        const int MINE_FLAG = -1;
        const int FLAG_FLAG = -2; //great naming
        const int QUESTION_FLAG = -3;
        const int HIDDEN_FLAG = -4;
        const int EXPLODED_FLAG = -5;

        private int remainingMines;
        private int revealed;

        private bool lost;
        public bool Won { get
            {
                return !lost && (ROWS * COLS == revealed + MINES);
            } }

        //If the value is -1, then it's a mine. Otherwise, it's the number of adjacent mines.
        private Tile[] board;

        public Minesweeper()
        {
            remainingMines = 0;
            revealed = 0;

            lost = false;

            buildBoard();
        }

        private int check(int x, int y)
        {
            if (x >= 0 && y >= 0 && x < COLS && y < ROWS)
                return board[x + y * COLS]?.Value ?? 0;
            else //edges
                return 0;
        }

        private void buildBoard()
        {
            var rng = new Random();
            board = new Tile[ROWS * COLS];
            int placed = 0;

            //Create mines
            do
            {
                var i = rng.Next(0, ROWS * COLS);
                if(board[i] is null)
                {
                    board[i] = new Tile(MINE_FLAG);
                    placed++;
                }
            } while (placed < MINES);

            //Calculate adjacents
            for(var x = 0; x < COLS; x++)
                for(var y = 0; y < ROWS; y++)
                {
                    if(check(x, y) != MINE_FLAG)
                    {
                        board[x + y * COLS] = new Tile(
                            (check(x, y + 1) == MINE_FLAG ? 1 : 0) //down
                          + (check(x - 1, y + 1) == MINE_FLAG ? 1 : 0) //down left
                          + (check(x + 1, y + 1) == MINE_FLAG ? 1 : 0) //down right
                          + (check(x, y - 1) == MINE_FLAG ? 1 : 0) //up
                          + (check(x - 1, y - 1) == MINE_FLAG ? 1 : 0) //up left
                          + (check(x + 1, y - 1) == MINE_FLAG ? 1 : 0) //up right
                          + (check(x - 1, y) == MINE_FLAG ? 1 : 0) //left
                          + (check(x + 1, y) == MINE_FLAG ? 1 : 0) //right
                        );
                    }
                }
        }

        //click a hidden tile
        //Returns true if it should update the clients
        public bool Click(int x, int y)
        {
            var tile = board[x + y * COLS];
            if (tile.Flagged || !tile.Hidden) return false;

            if (tile.Value == MINE_FLAG)
            {
                lost = true;
                tile.Exploded = true;
            }
            else
                reveal(x + y * COLS);

            return true;
        }

        private void reveal(int index)
        {
            revealed++;
            var x = index % COLS;
            var y = index / COLS;
            var tile = board[index];
            tile.Hidden = false;
            if (tile.Value == 0)
            {
                if (x > 0 && board[index - 1].Hidden) reveal(index - 1);                                        // left
                if (x < (COLS - 1) && board[+index + 1].Hidden) reveal(+index + 1);                                // right
                if (y < (ROWS - 1) && board[+index + COLS].Hidden) reveal(+index + COLS);                        // down
                if (y > 0 && board[index - COLS].Hidden) reveal(index - COLS);                                // up
                if (x > 0 && y > 0 && board[index - COLS - 1].Hidden) reveal(index - COLS - 1);                        // up & left
                if (x < (COLS - 1) && y < (ROWS - 1) && board[+index + COLS + 1].Hidden) reveal(+index + COLS + 1);        // down & right
                if (x > 0 && y < (ROWS - 1) && y < (ROWS - 1) && board[+index + COLS - 1].Hidden) reveal(+index + COLS - 1);                // down & left
                if (x < (COLS - 1) && y > 0 && y < (ROWS - 1) && board[+index - COLS + 1].Hidden) reveal(+index - COLS + 1);                // up & right
            }
        }

        public GameState GameState()
        {
            var gs = new GameState(ROWS, COLS);
            gs.Lost = lost;
            gs.Won = Won;
            for(var i = 0; i < board.Length; i++)
            {
                var tile = board[i];
                if (!lost)
                {
                    if (tile.Hidden)
                        gs[i] = tile.Flagged ? FLAG_FLAG : HIDDEN_FLAG;
                    else
                        gs[i] = tile.Value;
                }
                else gs[i] = tile.Exploded ? EXPLODED_FLAG : tile.Value;
            }
            return gs;
        }
    }
}
