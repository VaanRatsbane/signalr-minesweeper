﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalRChat.Logic.Minesweeper
{
    public class Tile
    {
        public int Value { get; set; }
        public bool Hidden { get; set; }
        public bool Flagged { get; set; }
        public bool Exploded { get; set; }

        public Tile(int value)
        {
            Value = value;
            Hidden = true;
            Flagged = false;
            Exploded = false;
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}
