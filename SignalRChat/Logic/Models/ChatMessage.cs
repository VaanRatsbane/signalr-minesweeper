﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalRChat.Logic.Models
{
    public class ChatMessage
    {
        public int UserId;
        public string Content;
        public DateTime Sent;
    }
}
