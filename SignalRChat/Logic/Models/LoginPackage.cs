﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalRChat.Logic.Models
{
    public class LoginPackage
    {
        public int ID;
        public IEnumerable<User> OnlineUsers;
        public IEnumerable<ChatMessage> MessageLog;
        public int MinesweeperCols;
        public int MinesweeperRows;
    }
}
