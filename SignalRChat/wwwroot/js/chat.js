﻿"use strict";

//adding console hooks to display in DOM
(function () {
	var oldLog = console.log;
	var oldError = console.error;
	var oldInfo = console.info;
	var oldWarn = console.warn;
	var logger = document.getElementById('logelement');
	console.log = function () {
		for (var i = 0; i < arguments.length; i++) {
			if (typeof arguments[i] === 'object') {
				logger.innerHTML += (JSON && JSON.stringify ? JSON.stringify(arguments[i], undefined, 2) : arguments[i]) + '<br />';
			} else {
				logger.innerHTML += arguments[i] + '<br />';
			}
		}
		oldLog.apply(console, arguments);
	};
	console.error = function () {
		for (var i = 0; i < arguments.length; i++) {
			if (typeof arguments[i] === 'object') {
				logger.innerHTML += (JSON && JSON.stringify ? JSON.stringify(arguments[i], undefined, 2) : arguments[i]) + '<br />';
			} else {
				logger.innerHTML += arguments[i] + '<br />';
			}
		}
		oldError.apply(console, arguments);
	};
	console.info = function () {
		for (var i = 0; i < arguments.length; i++) {
			if (typeof arguments[i] === 'object') {
				logger.innerHTML += (JSON && JSON.stringify ? JSON.stringify(arguments[i], undefined, 2) : arguments[i]) + '<br />';
			} else {
				logger.innerHTML += arguments[i] + '<br />';
			}
		}
		oldInfo.apply(console, arguments);
	};
	console.warn = function () {
		for (var i = 0; i < arguments.length; i++) {
			if (typeof arguments[i] === 'object') {
				logger.innerHTML += (JSON && JSON.stringify ? JSON.stringify(arguments[i], undefined, 2) : arguments[i]) + '<br />';
			} else {
				logger.innerHTML += arguments[i] + '<br />';
			}
		}
		oldWarn.apply(console, arguments);
	};
})();

//var connection = new signalR.HubConnectionBuilder().withUrl("/Secret/chatHub").build();
//if website is in 111.222.333.444:55555/WEBSITE_NAME, use .withUrl("/WEBSITE_NAME/chatHub")
var connection = new signalR.HubConnectionBuilder().withUrl("/chatHub").build();

//Variables to keep track off
var myUserId = null;
var onlineUsers;
var tiles;
var totalCols;
var totalRows;

var findUser = function (userId) {
	return onlineUsers.find(c => c.id === +userId);
};

//Create event functions
var receiveMessageFunc = function (userId, message, datetime) {
	const user = findUser(userId);
	if (user) {
		var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
		var date = new Date(datetime);
		
		var encodedMsg = `[${date.toTimeString().slice(0, 8)}] ${user.name}\: ${msg}`;
		var li = document.createElement("li");
		li.textContent = encodedMsg;
		document.getElementById("messagesList").appendChild(li);
	}
};

var loginSuccessFunc = function (loginPkg) {
	connection.off('LoginSuccess', loginSuccessFunc);
	connection.on("ReceiveMessage", receiveMessageFunc);
	connection.on('UpdateGameState', updateGameState);
	myUserId = loginPkg['id'];
	onlineUsers = loginPkg['onlineUsers'];
	for (let i = 0; i < loginPkg['messageLog'].length; i++) {
		const msg = loginPkg['messageLog'][i];
		receiveMessageFunc(+msg.userId, msg.content, msg.sent);
	}
	document.getElementById("loginBox").hidden = true;
	document.getElementById("mainContent").hidden = false;

	createBoard(loginPkg.minesweeperCols, loginPkg.minesweeperRows);
};

var clickedBoard = function (event) {
	let source = event.target;
	let id = +source.id;                        // The ID of the tile clicked by user.

	//only do something if it's a closed tile
	if (tiles[id].tileType === "images/x.png") {
		let x = id % totalCols;
		let y = Math.floor(id / totalCols);
		if (event.button === 2) {
			console.log(`Right Click tile ${x}|${y}`);
			event.preventDefault(); //stop right click popup
		}
		else if (event.button === 0) {
			console.log(`Left Click tile ${x}|${y}`);
			connection.invoke("LeftClick", myUserId, x, y).catch(function (err) {
				return console.error(err.toString());
			});
		}
	}
};

var createBoard = function (cols, rows) {
	totalCols = cols;
	totalRows = rows;
	tiles = [];
	var gameboard = document.getElementById("gameboard");
	for (let i = 0; i < rows * cols; i++) // Create the tiles.
	{
		tiles[i] = document.createElement('img');        // Each tile is an HTML image.
		tiles[i].src = tiles[i].tileType = "images/x.png";                        // Initial picture: uncovered tile.
		tiles[i].style = "position:absolute;height:30px; width: 30px";
		tiles[i].style.top = `${20 + Math.floor(i / cols) * 30}px`;        // Place the tile vertically
		tiles[i].style.left = `${20 + i % cols * 30}px`;                // and horizontally.
		tiles[i].addEventListener('mousedown', clickedBoard);        // Function 'click' will be executed when player clicks on a tile.
		tiles[i].id = i;                                        // The id of the tile is its index.
		gameboard.appendChild(tiles[i]);                // Add the tile to the DOM.
	}
};

var updateGameState = function (state) {
	revealBoard(state.boardView);
	if (state.lost) {
		return;
	} else if (state.won) {
		return;
	}
};

var revealBoard = function (boardView) {
	for (let i = 0; i < boardView.length; i++) {
		const value = +boardView[i];
		switch (value) {
			case -1: tiles[i].src = tiles[i].tileType = "images/m.png"; break;
			case -2: tiles[i].src = tiles[i].tileType = "images/f.png"; break;
			case -3: tiles[i].src = tiles[i].tileType = "images/q.png"; break;
			case -4: tiles[i].src = tiles[i].tileType = "images/x.png"; break;
			case -5: tiles[i].src = tiles[i].tileType = "images/e.png"; break;
			default: tiles[i].src = tiles[i].tileType = `images/${value}.png`; break;
		}
	}
};

//Disable send button until connection is established
//document.getElementById("sendButton").disabled = true;

//connection.on("ReceiveMessage", receiveMessageFunc);
//connection.off("ReceiveMessage", receiveMessageFunc);

connection.start().then(function () {
	document.getElementById("sendButton").disabled = false;
}).catch(function (err) {
	return console.error(err.toString());
});

document.getElementById("loginButton").addEventListener("click", function (event) {
	var username = document.getElementById("userInput").value;
	if (username) {
		connection.on('LoginSuccess', loginSuccessFunc);
		connection.invoke("Login", username).catch(function (err) {
			return console.error(err.toString());
		});
		event.preventDefault();
	}
});

document.getElementById("sendButton").addEventListener("click", function (event) {
	if (!myUserId) return;
	var msgEl = document.getElementById("messageInput");
	var message = msgEl.value;
	if (message) {
		msgEl.value = '';
		connection.invoke("SendMessage", myUserId, message).catch(function (err) {
			return console.error(err.toString());
		});
		event.preventDefault();
	}
});